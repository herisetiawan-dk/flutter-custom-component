import 'package:dynamic_component/common/constants/component_id_constants.dart';

class MockComponentConstants {
  static final Map<String, dynamic> cardComponent = {
    'component': ComponentIdConstants.cardComponent,
    'header': {
      'title': 'Angsuran sudah dibayar',
      'expandable': true,
      'initExpanded': true,
    },
    'contents': [
      {
        'component': ComponentIdConstants.rowComponent,
        'contents': [
          {
            'component': ComponentIdConstants.textComponent,
            'value': 'Rp5.393.000',
            'fontWeight': 'w700',
            'fontSize': 20.0
          },
          {
            'component': ComponentIdConstants.textComponent,
            'value': ' dari Rp129.432.000',
            'fountWeight': 'w200',
          }
        ]
      },
      {
        'component': ComponentIdConstants.progressComponent,
        'range': 100,
        'current': 72,
      },
      {
        'component': ComponentIdConstants.rowComponent,
        'contents': [
          {
            'component': ComponentIdConstants.textComponent,
            'value': '14 dari 24',
            'fontWeight': 'w700',
            'fontSize': 12.0,
          },
          {
            'component': ComponentIdConstants.textComponent,
            'value': ' sudah dibayar',
            'fountWeight': 'w200',
            'fontSize': 12.0
          }
        ]
      },
      {
        'component': ComponentIdConstants.horizontalLineComponent,
      },
      {
        'component': ComponentIdConstants.rowComponent,
        'alignment': 'space_between',
        'contents': [
          {
            'component': ComponentIdConstants.textComponent,
            'value': 'Bukti pembayaran sebelumnya',
            'fontWeight': 'w600',
            'fontSize': 14.0,
          },
          {
            'component': ComponentIdConstants.clickableComponent,
            'clickAction': 'open_drawer',
            'clickArgument': {'success': true},
            'content': {
              'component': ComponentIdConstants.expandIconComponent,
            },
          },
        ]
      }
    ],
  };
}
