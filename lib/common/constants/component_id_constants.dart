class ComponentIdConstants {
  static const String cardComponent = 'card_component';
  static const String progressComponent = 'progress_component';
  static const String rowComponent = 'row_component';
  static const String textComponent = 'text_component';
  static const String horizontalLineComponent = 'horizontal_line_component';
  static const String expandIconComponent = 'expand_icon_component';
  static const String clickableComponent = 'clickable_component';
}