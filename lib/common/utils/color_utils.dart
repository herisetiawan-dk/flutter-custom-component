import 'package:flutter/material.dart';

class ColorUtils {
  static Color? fromString(String? value) {
    final colorValue = int.tryParse(value ?? '');
    if (colorValue != null) return Color(colorValue);
    return null;
  }
}
