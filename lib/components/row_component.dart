import 'package:dynamic_component/common/constants/component_id_constants.dart';
import 'package:dynamic_component/components/base_custom_component.dart';
import 'package:dynamic_component/components/custom_components.dart';
import 'package:flutter/material.dart';

class RowArgument extends BaseComponentArgument {
  final String? alignment;

  RowArgument({
    required super.component,
    required super.contents,
    this.alignment,
  });

  static RowArgument fromJson(dynamic json) => RowArgument(
        component: json['component'],
        alignment: json['alignment'],
        contents: json['contents'],
      );
}

class RowComponent extends BaseCustomComponent<RowArgument> {
  @override
  String get componentId => ComponentIdConstants.rowComponent;

  @override
  RowArgument argumentsFromJson(json) => RowArgument.fromJson(json);

  MainAxisAlignment getAlignment(String? value) {
    switch (value) {
      case 'center':
        return MainAxisAlignment.center;
      case 'space_around':
        return MainAxisAlignment.spaceAround;
      case 'space_between':
        return MainAxisAlignment.spaceBetween;
      case 'space_evenly':
        return MainAxisAlignment.spaceEvenly;
      case 'end':
        return MainAxisAlignment.end;
      default:
        return MainAxisAlignment.start;
    }
  }

  @override
  Widget render(BuildContext context) => Row(
      mainAxisAlignment: getAlignment(arguments.alignment),
      children: arguments.contents
          .map<Widget>(
            (component) => CustomComponents.get(component).render(
              context,
            ),
          )
          .toList(),
    );
}
