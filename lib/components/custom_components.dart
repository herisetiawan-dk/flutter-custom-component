import 'package:dynamic_component/components/base_custom_component.dart';
import 'package:dynamic_component/components/card_component.dart';
import 'package:dynamic_component/components/clickable_component.dart';
import 'package:dynamic_component/components/expand_icon_component.dart';
import 'package:dynamic_component/components/horizontal_line_component.dart';
import 'package:dynamic_component/components/progress_component.dart';
import 'package:dynamic_component/components/row_component.dart';
import 'package:dynamic_component/components/text_component.dart';

class CustomComponents {
  static List<BaseCustomComponent> components = [
    CardComponent(),
    ProgressComponent(),
    TextComponent(),
    RowComponent(),
    HorizontalLineComponent(),
    ExpandIconComponent(),
    ClickableComponent(),
  ];

  static List<BaseCustomComponent> getAll() => components;

  static BaseCustomComponent get(dynamic arguments) => (components).firstWhere(
          (component) => component.componentId == arguments['component'],
          orElse: () {
        throw UnknownCustomComponent(arguments['component']);
      }).withArguments(arguments);
}

class UnknownCustomComponent extends AssertionError {
  final dynamic componentId;

  UnknownCustomComponent(this.componentId);

  @override
  Object get message =>
      'Unknown componentId: $componentId, make sure component registered in CustomComponents.components';
}
