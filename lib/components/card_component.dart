import 'package:dynamic_component/common/constants/component_id_constants.dart';
import 'package:dynamic_component/components/base_custom_component.dart';
import 'package:flutter/material.dart';

import 'custom_components.dart';

class CardHeaderArgument {
  final String title;
  final bool? initExpanded;
  final bool? expandable;

  CardHeaderArgument({
    required this.title,
    this.initExpanded,
    this.expandable,
  });

  static CardHeaderArgument fromJson(dynamic json) => CardHeaderArgument(
        title: json['title'],
        initExpanded: json['initExpanded'],
        expandable: json['expandable'],
      );
}

class CardArgument extends BaseComponentArgument {
  final CardHeaderArgument? header;

  CardArgument({
    super.component,
    super.contents,
    super.content,
    this.header,
  });

  static CardArgument fromJson(dynamic json) => CardArgument(
        component: json['component'],
        contents: json['contents'],
        content: json['content'],
        header: json['header'] == null
            ? json['header']
            : CardHeaderArgument.fromJson(json['header']),
      );
}

class CardComponent extends BaseCustomComponent<CardArgument> {
  @override
  String get componentId => ComponentIdConstants.cardComponent;

  @override
  CardArgument argumentsFromJson(json) => CardArgument.fromJson(json);

  @override
  Widget render(context) {
    ValueNotifier<bool> expandedListener = ValueNotifier(
      arguments.header?.initExpanded ?? true,
    );

    return AnimatedContainer(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16.0),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              offset: const Offset(1, 1),
              spreadRadius: 1.5,
              blurRadius: 5,
            )
          ]),
      padding: const EdgeInsets.all(16.0),
      duration: const Duration(milliseconds: 200),
      child: ValueListenableBuilder<bool>(
        valueListenable: expandedListener,
        builder: (context, expanded, _) => Column(
          children: [
            if (arguments.header != null)
              Row(
                children: [
                  Expanded(
                    child: Text(
                      arguments.header!.title,
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  if (arguments.header!.expandable != null)
                    AnimatedRotation(
                      duration: const Duration(milliseconds: 200),
                      turns: expanded ? 90 / 360 : 0,
                      child: GestureDetector(
                        onTap: () => expandedListener.value = !expanded,
                        child: const Icon(
                          Icons.arrow_forward_ios_rounded,
                          size: 16,
                        ),
                      ),
                    ),
                ],
              ),
            if (expanded) ...[
                ...arguments.contents
                    .map<Widget>(
                      (component) => Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4.0),
                        child: CustomComponents.get(component).render(context),
                      ),
                    )
                    .toList(),
              // TODO: handle footer
            ],
          ],
        ),
      ),
    );
  }
}
