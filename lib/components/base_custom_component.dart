import 'package:flutter/material.dart';

abstract class BaseComponentArgument {
  final String? component;
  final List<dynamic> contents;
  final dynamic content;

  BaseComponentArgument({
    required this.component,
    this.content,
    this.contents = const [],
  });
}

abstract class BaseCustomComponent<T extends BaseComponentArgument> {
  abstract final String componentId;
  late T arguments;

  withArguments(dynamic json) {
    arguments = argumentsFromJson(json);
    return this;
  }

  T argumentsFromJson(dynamic json);

  Widget render(BuildContext context);
}
