import 'package:dynamic_component/common/constants/component_id_constants.dart';
import 'package:dynamic_component/components/base_custom_component.dart';
import 'package:dynamic_component/components/custom_components.dart';
import 'package:flutter/material.dart';

class ClickableArgument extends BaseComponentArgument {
  final String clickAction;
  final dynamic clickArgument;

  ClickableArgument({
    required super.component,
    required super.content,
    required this.clickAction,
    this.clickArgument,
  });

  static ClickableArgument fromJson(dynamic json) => ClickableArgument(
        component: json['component'],
        content: json['content'],
        clickAction: json['clickAction'],
        clickArgument: json['clickArgument'],
      );
}

class ClickableComponent extends BaseCustomComponent<ClickableArgument> {
  @override
  String get componentId => ComponentIdConstants.clickableComponent;

  @override
  ClickableArgument argumentsFromJson(json) => ClickableArgument.fromJson(json);

  void Function()? getAction(String action, dynamic argument) {
    switch (action) {
      case 'open_drawer':
        return () => print('Opening drawer with arguments: $argument');
      default:
        return null;
    }
  }

  @override
  Widget render(BuildContext context) => InkWell(
        onTap: getAction(arguments.clickAction, arguments.clickArgument),
        child: CustomComponents.get(arguments.content).render(context),
      );
}
