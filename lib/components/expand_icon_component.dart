import 'package:dynamic_component/common/constants/component_id_constants.dart';
import 'package:dynamic_component/components/base_custom_component.dart';
import 'package:flutter/material.dart';

class ExpandIconArgument extends BaseComponentArgument {
  ExpandIconArgument({required super.component});

  static ExpandIconArgument fromJson(dynamic json) => ExpandIconArgument(
        component: json['component'],
      );
}

class ExpandIconComponent extends BaseCustomComponent<ExpandIconArgument> {
  @override
  String get componentId => ComponentIdConstants.expandIconComponent;

  @override
  ExpandIconArgument argumentsFromJson(json) =>
      ExpandIconArgument.fromJson(json);

  @override
  Widget render(BuildContext context) => const Icon(
      Icons.arrow_forward_ios_rounded,
      size: 16,
    );
}
