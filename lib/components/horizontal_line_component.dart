import 'package:dynamic_component/common/constants/component_id_constants.dart';
import 'package:dynamic_component/components/base_custom_component.dart';
import 'package:flutter/material.dart';

class HorizontalLineArgument extends BaseComponentArgument {
  HorizontalLineArgument({required super.component});

  static HorizontalLineArgument fromJson(dynamic json) =>
      HorizontalLineArgument(
        component: json['component'],
      );
}

class HorizontalLineComponent
    extends BaseCustomComponent<HorizontalLineArgument> {
  @override
  String get componentId => ComponentIdConstants.horizontalLineComponent;

  @override
  HorizontalLineArgument argumentsFromJson(json) =>
      HorizontalLineArgument.fromJson(json);

  @override
  Widget render(BuildContext context) => LayoutBuilder(
      builder: (context, constraints) => Container(
        height: 1,
        width: constraints.maxWidth,
        color: Colors.grey.withOpacity(0.5),
      ),
    );
}
