import 'package:dynamic_component/common/constants/component_id_constants.dart';
import 'package:dynamic_component/components/base_custom_component.dart';
import 'package:flutter/material.dart';

class TextArgument extends BaseComponentArgument {
  final String value;
  final double? fontSize;
  final String? fontWeight;

  TextArgument({
    required super.component,
    required this.value,
    this.fontSize,
    this.fontWeight,
  });

  static TextArgument fromJson(dynamic json) => TextArgument(
        component: json['component'],
        value: json['value'],
        fontSize: json['fontSize'] as double?,
        fontWeight: json['fontWeight'],
      );
}

class TextComponent extends BaseCustomComponent<TextArgument> {
  @override
  String get componentId => ComponentIdConstants.textComponent;

  @override
  TextArgument argumentsFromJson(json) => TextArgument.fromJson(json);

  FontWeight? getFontWeight(String? value) {
    switch (value) {
      case 'w100':
        return FontWeight.w100;
      case 'w200':
        return FontWeight.w200;
      case 'w300':
        return FontWeight.w300;
      case 'w400':
        return FontWeight.w400;
      case 'w500':
        return FontWeight.w500;
      case 'w600':
        return FontWeight.w600;
      case 'w700':
        return FontWeight.w700;
      case 'w800':
        return FontWeight.w800;
      case 'w900':
        return FontWeight.w900;
      case 'bold':
        return FontWeight.bold;
      default:
        return FontWeight.normal;
    }
  }

  @override
  Widget render(BuildContext context) => Text(
      arguments.value,
      style: TextStyle(
        fontSize: arguments.fontSize,
        fontWeight: getFontWeight(
          arguments.fontWeight,
        ),
      ),
    );
}
