import 'package:dynamic_component/common/constants/component_id_constants.dart';
import 'package:dynamic_component/common/utils/color_utils.dart';
import 'package:dynamic_component/components/base_custom_component.dart';
import 'package:flutter/material.dart';

class ProgressArgument extends BaseComponentArgument {
  final num range;
  final num current;
  final Color? color;

  ProgressArgument({
    required super.component,
    required this.range,
    required this.current,
    this.color,
  });

  static ProgressArgument fromJson(dynamic json) => ProgressArgument(
        component: json['component'],
        range: json['range'],
        current: json['current'],
        color: ColorUtils.fromString(json['color']),
      );
}

class ProgressComponent extends BaseCustomComponent<ProgressArgument> {
  @override
  String get componentId => ComponentIdConstants.progressComponent;

  @override
  ProgressArgument argumentsFromJson(json) => ProgressArgument.fromJson(json);

  @override
  Widget render(BuildContext context) => LayoutBuilder(
      builder: (context, constraints) {
        final num range = arguments.range;
        final num current = arguments.current;
        final num percent = current / range;
        final filledWidth = constraints.maxWidth * percent;
        return ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: SizedBox(
            height: 10,
            width: constraints.maxWidth,
            child: Row(
              children: [
                Container(
                  width: filledWidth,
                  color: Colors.amber,
                ),
                Expanded(
                  child: Container(
                    color: Colors.grey.withOpacity(0.5),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
}
