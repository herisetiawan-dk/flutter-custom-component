import 'package:dynamic_component/common/constants/mock_constants.dart';
import 'package:dynamic_component/components/custom_components.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final components = [
    MockComponentConstants.cardComponent,
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.white),
        useMaterial3: true,
      ),
      home: Scaffold(
        backgroundColor: const Color(0xfafafaff),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: components
                  .map<Widget>(
                    (component) =>
                        CustomComponents.get(component).render(context),
                  )
                  .toList(),
            ),
          ),
        ),
      ),
    );
  }
}
